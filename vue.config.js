module.exports = {
  publicPath: './',

  devServer: {
    proxy: {
      '/ajax': {
        target: 'http://m.maoyan.com',
        changeOrigin: true
      },
      '/dianying': {
        target: 'http://m.maoyan.com',
        changeOrigin: true
      }
    }
  },
  css: {
    extract: false
  }
}
