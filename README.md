# VUE 仿写猫眼电影

用 vue 仿写猫眼电影网站界面

## 开发组件

- typescript、sass
- vue、vuecli、vue-router、vuex、vue-property-decorator
- axios、iscroll

## 在线预览：

腾讯云 nginx [134.175.80.113](http://134.175.80.113/maoyan)  
预览请使用 DevTools 的手机模式

## 项目结构

```
├───img                     ----readme.md图片
├───public
│       index.html          ----webpack打包出口
│
└───src
    │   App.vue                 ----vue入口
    │   main.ts                 ----nodejs入口
    │   router.ts               ----路由
    │   store.ts                ----状态管理
    │
    ├───assets                  ----图片资源文件
    │
    ├───components          ----组件
    │   │   Content.vue         ----滑动容器
    │   │   drop.vue            ----下拉框
    │   │   Header.vue          ----头部
    │   │   Scroll.vue          ----滑动
    │   │
    │   ├───movie
    │   │       Coming.vue      ----待映
    │   │       Playing.vue     ----热映
    │   │
    │   └───search          ----搜索页面组件
    │           SearchCinema.vue----搜索影院
    │           SearchMovie.vue ----搜索电影
    │
    ├───service
    │       api.ts              ----调用的api
    │       cinemaService.js    ----影院请求数据处理
    │       http.ts             ----包装axios
    │       movieService.js     ----电影数据请求处理
    │       otherService.js     ----其他数据请求处理
    │
    └───views
        │   cinema.vue          ----影院
        │   movie.vue           ----电影
        │   user.vue            ----用户
        │
        └───common
                cinemaList.vue  ----影院列表
                cinemaShow.vue  ----影院信息
                CityList.vue    ----城市列表
                detail.vue      ----电影详情
                search.vue      ----搜索
```

## 预览图片

![img](https://github.com/fokq/maoyanFilm/raw/master/img/1.png)
![img](https://github.com/fokq/maoyanFilm/raw/master/img/2.png)
![img](https://github.com/fokq/maoyanFilm/raw/master/img/3.png)
![img](https://github.com/fokq/maoyanFilm/raw/master/img/4.png)
![img](https://github.com/fokq/maoyanFilm/raw/master/img/5.png)
![img](https://github.com/fokq/maoyanFilm/raw/master/img/6.png)
![img](https://github.com/fokq/maoyanFilm/raw/master/img/7.png)
