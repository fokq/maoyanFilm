const PLAYING_API = '/ajax/movieOnInfoList'

// 即将上映
const COMING_API = '/ajax/comingList'

// 更多正在热映
const MORE_PLAYING_API = '/ajax/moreComingList'

const MORE_COMING_API = './ajax/moreComingList'

//最受期待的电影
const MOST_EXPECTED_API = '/ajax/mostExpected'

// 城市页面
const CITY_API = '/dianying/cities.json'

const SEARCH_API = '/ajax/search'

const DETAIL_MOVIE_API = '/ajax/detailmovie'

// 获取正在热映的电影的详情信息
const PLAYING_DETAIL_API = '/ajax/detailmovie'

// forceUpdate=1543022891563
const MOVIE_DETAIL_API = '/ajax/movie'

export default {
  PLAYING_API,
  MORE_PLAYING_API,
  COMING_API,
  MORE_COMING_API,
  MOST_EXPECTED_API,
  CITY_API,
  SEARCH_API,
  DETAIL_MOVIE_API,
  PLAYING_DETAIL_API,
  MOVIE_DETAIL_API
}
