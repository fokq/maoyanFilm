import http from './http'
import API from './api'

const imgReplace = (item: any) => {
  item.img = item.img.replace(/w.h/, '128.180')
  return item
}

//请求正在热映的电影数据
export async function getPlayingList() {
  const { data, status } = await http({ url: API.PLAYING_API, data: { token: '' } })
  if (status != 200) return
  return { data: data.movieList.map(imgReplace), ids: data.movieIds }
}

//请求更多正在热映的电影数据
export async function getMorePlayingList(movieIds: any) {
  const { data, status } = await http({ url: API.MORE_PLAYING_API, data: { token: '', movieIds } })
  if (status != 200) return
  return data.coming.map(imgReplace)
}

//请求即将上映的电影数据
export async function getComingList(ci: any) {
  const { data, status } = await http({
    url: API.COMING_API,
    data: { ci, token: '', limit: 10 }
  })
  if (status != 200) return
  //过滤数据
  const newData = data.coming.map(imgReplace)
  //对数据进行分类
  const dataMap: any = []
  newData.map((item: any) => {
    if (!dataMap.hasOwnProperty(item.comingTitle)) {
      dataMap[item.comingTitle] = []
    }
    dataMap[item.comingTitle].push(item)
  })
  return { dataResult: newData, data: dataMap, ids: data.movieIds }
}

//请求更多即将上映的电影数据
export async function getMoreComingList(ci: any, movieIds: any) {
  const { data, status } = await http({
    url: API.MORE_COMING_API,
    data: { ci, movieIds, token: '', limit: 10 }
  })
  if (status != 200) return
  return data.coming.map(imgReplace)
}

// 请求最受期待的电影数据
export async function getMostExpectedData(ci: any) {
  const { data, status } = await http({
    url: API.MOST_EXPECTED_API,
    data: { ci, limit: 10, offset: 0, token: '' }
  })
  if (status != 200) return
  return data.coming.map((item: any) => {
    item.comingTitle = item.comingTitle.split(' ')[0]
    return item
  })
}

// 请求更多最受期待的电影数据
export function getMoreMostExpectedData(num: any) {
  return new Promise(resolve => {
    http({
      url: API.MOST_EXPECTED_API,
      data: { ci: 30, limit: 10, offset: num, token: '' }
    }).then(({ data, status }) => {
      if (status != 200) return

      let newMoreData = data.coming.map((item: any) => {
        item.comingTitle = item.comingTitle.split(' ')[0]
        return item
      })
      //请求成功
      resolve(newMoreData)
    })
  })
}

// 请求正在播放详情页
export function getPlayingDetail(movieId: any) {
  return new Promise(resolve => {
    http({
      url: API.PLAYING_DETAIL_API,
      data: { movieId }
    }).then(({ data, status }) => {
      if (status != 200) return

      data.detailMovie.img = data.detailMovie.img.replace(/w.h/, '128.180')
      data.detailMovie.snum = (data.detailMovie.snum / 10000).toFixed(1)
      resolve(data)
    })
  })
}

// 请求电影详情
export function getMovieDetail(movieId: any, time: any, cityId: any) {
  return new Promise(resolve => {
    http({
      url: API.MOVIE_DETAIL_API,
      method: 'POST',
      data: {
        movieId,
        offset: 0,
        limit: 20,
        districtId: -1,
        lineId: -1,
        hallType: -1,
        brandId: -1,
        serviceId: -1,
        areaId: -1,
        stationId: -1,
        updateShowDay: true,
        reqId: time,
        cityId,
        forceUpdate: 15431484
      }
    }).then(({ data, status }) => {
      if (status != 200) return
      resolve(data)
    })
  })
}
