import { get } from './http'

//请求正在热映的电影数据
export async function getCinemaList(cityId: number) {
  const { data, status } = await get('/ajax/cinemaList', {
    day: '2019-03-17',
    offset: 0,
    limit: 100,
    districtId: -1,
    lineId: -1,
    hallType: -1,
    brandId: -1,
    serviceId: -1,
    areaId: -1,
    stationId: -1,
    item: '',
    updateShowDay: true,
    reqId: 1542442758746,
    cityId
  })
  if (status === 200) return data
}

//请求正在热映的电影数据
export async function getFilter() {
  const { data, status } = await get('/ajax/filterCinemas', { cityId: 30 })
  if (status === 200) return data
}

//请求正在热映的电影数据
export async function getShows(cinemaId: number) {
  const { data, status } = await get('/ajax/cinemaDetail', { cinemaId })
  if (status === 200) return data
}
