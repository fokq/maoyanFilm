import API from './api'
import http from './http'

//获取城市
export async function getCityList() {
  const { data, status } = await http({ url: API.CITY_API })
  if (status !== 200) return
  // 取得首字母，按字母先分类
  let map: any
  data.cts.map((item: any) => {
    const letter = item.py[0]
    if (!map[letter]) map[letter] = []
    map[letter].push(item)
  })

  let keys = Object.keys(map)
  // 选择排序
  for (let i = 0; i < keys.length; i++) {
    for (let j = i + 1; j < keys.length; j++) {
      if (keys[i] > keys[j]) {
        ;[keys[i], keys[j]] = [keys[j], keys[i]]
      }
    }
  }

  return {
    keys,
    data: keys.map(item => {
      return {
        key: item,
        value: map[item]
      }
    })
  }
}

// 搜索影院和电影的接口
export async function getCinemaAndMovie(cityId: any, kw: any) {
  const { data, status } = await http({
    url: API.SEARCH_API,
    data: { cityId, kw, stype: -1 }
  })

  if (status != 200) return

  let newCinemaData = null
  let newMovieData = []
  let movieTotal = ''
  let cinemaTotal = ''

  // 获取影院
  if (data.cinemas) {
    newCinemaData = data.cinemas.list
    cinemaTotal = data.cinemas.total
  }

  // 获取电影
  if (data.movies) {
    newMovieData = data.movies.list.map((item: any) => {
      item.img = item.img.replace(/w.h/, '128.180')
      return item
    })
    movieTotal = data.movies.total
  }

  return { newCinemaData, newMovieData, movieTotal, cinemaTotal }
}
