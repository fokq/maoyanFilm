import axios, { Method } from 'axios'

export default function http(obj: { url: string; method?: Method; data?: any }) {
  const { url, method, data } = obj
  const options = {
    url,
    method,
    withCredentials: false,
    params: undefined,
    data: undefined
  }

  if (method === 'POST') {
    options.data = data
  } else {
    options.params = data
  }

  return axios(options)
}

export function get(url: string, data?: any) {
  return axios({
    url,
    withCredentials: false,
    params: data
  })
}
