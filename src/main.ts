import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import '@/style/style.css'

Vue.config.productionTip = false

import VueCompositionApi from '@vue/composition-api'
Vue.use(VueCompositionApi)

declare module '@vue/composition-api/dist/component/component' {
  interface SetupContext {
    readonly refs: { [key: string]: Vue | Element | Vue[] | Element[] }
  }
}

Vue.filter('replaceWH', (value: any, w: number, h: number) => {
  return value.replace(/w.h/, `${w}.${h}`)
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
