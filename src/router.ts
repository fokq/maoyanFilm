import Vue from 'vue'
import Router from 'vue-router'

import Movie from './views/movie/index.vue'
const Detail = () => import('./views/movie/detail.vue')
const Cinema = () => import('./views/cinema/index.vue')
const cinemaShow = () => import('./views/cinema/cinemaShow.vue')
const User = () => import('./views/user.vue')

const CityList = () => import('./views/common/CityList.vue')
const Search = () => import('./views/common/search.vue')

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/movie',
      component: Movie,
      children: [
        {
          path: 'search',
          component: Search,
          props: {
            type: ['cinema', 'movie']
          }
        },
        {
          path: 'city-list',
          component: CityList
        },
        {
          path: 'detail',
          component: Detail
        }
      ]
    },
    {
      path: '/cinema',
      component: Cinema,
      children: [
        {
          path: 'SearchCinema',
          component: Search,
          props: {
            type: ['cinema', 'movie']
          }
        },
        {
          path: 'show',
          component: cinemaShow
        },
        {
          path: 'city-list',
          component: CityList
        }
      ]
    },
    {
      path: '/user',
      component: User
    },
    {
      path: '**',
      redirect: '/movie'
    }
  ]
})
